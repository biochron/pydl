import os
import sys
import time
import datetime
import argparse
import pandas as pd
from mpi4py import MPI
import numpy as np


def gen_wave(times, period, kind):
    num_time = len(times)
    wave_vec = np.empty(num_time, dtype=float)
    w = 2 * np.pi / period
    for i in range(num_time):
        wave_vec[i] = kind(w * times[i])

    return wave_vec


def rand_curve_from_background(data_df):
    num_var, num_time = data_df.shape
    # draw random curve indices from which to draw curve values
    rand_idx = np.random.randint(0, num_var, size=num_time)
    # generate a random curve from the background distribution
    rand_curve = np.empty(num_time, dtype=float)
    for i in range(num_time):
        rand_curve[i] = data_df[rand_idx[i], i]

    return rand_curve


def p_reg(curve, reg_cdf):
    num_reg = len(reg_cdf)
    # determine the fraction of random curves with standard deviation no smaller than the stdev of a given curve
    return (num_reg - np.searchsorted(reg_cdf, curve.std())) / num_reg


def gen_per_cdf(curve, num_per, sin_vec, cos_vec):
    # generate num_per random curves from the given curve and compute their periodicity scores
    per_cdf = np.empty(num_per, dtype=float)
    for i in range(num_per):
        per_cdf[i] = per_score(np.random.permutation(curve), sin_vec, cos_vec)
    per_cdf.sort()

    return per_cdf


def per_score(curve, sin_vec, cos_vec):
    # compute the periodicity score
    return np.sqrt(np.dot(sin_vec, curve) ** 2 + np.dot(cos_vec, curve) ** 2)


def p_per(curve, per_cdf, sin_vec, cos_vec):
    num_per = len(per_cdf)
    # determine the fraction of random curves with periodicity score no smaller than the per score of a given curve
    return (num_per - np.searchsorted(per_cdf, per_score(curve, sin_vec, cos_vec))) / num_per
