"""
This Python module implements the periodicity detection algorithm of de Lichtenberg et. al.
(https://academic.oup.com/bioinformatics/article/21/7/1164/268864)
used to identify rhythmicity in gene expression profile time series data.
"""

from utilities import *

FPATH = os.path.dirname(os.path.realpath(__file__)), [0]
VERSION = "0.3.0"

if __name__ == '__main__':
    """
    pyDL requires as input a tab-delimited text file with columns indicating time points and rows indicating genes.  
    Command-line parameters which may be optionally specified include:
        -r, --num_reg: int >= 1 specifying the number of random curves to generate from the set of all curves to 
                       compute an empirical regulation p-value (default: 1,000,000)
        -p, --num_per: int >= 1 specifying the number of random curves to generate from each fixed curve to 
                       compute an empirical periodicity p-value (default: 100,000)
        -l, --log_trans: bool specifying if data requires log transform, (detail: True)
    """

    parser = argparse.ArgumentParser(prog='pyDL v0.3.0',
                                     description='deLichtenberg periodicity detection algorithm')

    # - required command-line argument -
    parser.add_argument('data_files',
                        action='store',
                        help='file from which to read data')
    parser.add_argument('-T', '--periods',
                        type=str,
                        required=True,
                        help='string specifying periods i.e. "1,3,5,7"')
    parser.add_argument('-o', '--output',
                        type=str,
                        default=None,
                        required = True,
                        help='path to output file to save results')
    # - optional command-line arguments -
    parser.add_argument('-r', '--num_reg',
                        type=int,
                        default=1000000,
                        help='number of random curves for empirical regulation p-value')
    parser.add_argument('-p', '--num_per',
                        type=int,
                        default=100000,
                        help='number of random curves for empirical periodicity p-value')
    parser.add_argument('-l', '--log_trans',
                        type=bool,
                        default=True,
                        help='bool flag indicating if data requires log transform')
    parser.add_argument('-v', '--verbose',
                        type=bool,
                        default=False,
                        help='bool flag indicating if progress should be displayed')
    args = parser.parse_args()

    start_time = datetime.datetime.now()

    # load time series data
    data_files = args.data_files
    data_df = pd.read_csv(data_files, delimiter='\t', header=0, index_col=0, comment='#')
    num_var = len(data_df.index.values)
    times = np.array([float(time) for time in data_df.columns.values])

    comm = MPI.COMM_WORLD
    num_proc = comm.Get_size()
    rank = comm.Get_rank()
    status = MPI.Status()

    tags = {'ASK_FOR_WORK': 0, 'WORK': 1, "WORK_DONE": 2, "DIE": 3}
    # initialize reg_cdf array
    reg_cdf = 0
    periods = 0

    # master
    if rank == 0:

        # parse period array
        periods = [float(x) for x in args.periods.split(',')]

        if args.verbose:
            print('periods: ', periods)

        # optionally preprocess data
        if args.log_trans:
            data_df = data_df.apply(lambda x: np.log2(x / x.mean() + 1),
                                    axis=1)  # ensure each value is at least 1 before log transform

        # generate empirical regulation distribution from background
        if args.verbose:
            print('generating random curves from background')

        # convert df to np.array and make C-continuous
        data_array = data_df.values.copy(order='C')
        # initialize reg_cdf array
        reg_cdf = np.zeros(args.num_reg, dtype=float)
        # number of generations performed
        rand_gens = 0
        dead_workers = 0

        while dead_workers < num_proc - 1:
            # master looking for workers
            data = comm.recv(source=MPI.ANY_SOURCE, tag=MPI.ANY_TAG, status=status)
            dest = status.Get_source()

            if status.Get_tag() == tags["ASK_FOR_WORK"]:
                if rand_gens <= args.num_reg - 1:
                    # master got request for work from worker
                    # master sending work to worker
                    comm.send(data_array, dest=dest, tag=tags["WORK"])
                    # master sent work to worker
                else:
                    # Someone did more work than they should have
                    # master telling worker to die
                    comm.send(data_array, dest=dest, tag=tags["DIE"])
                    dead_workers += 1

            elif status.Get_tag() == tags["WORK_DONE"]:
                if rand_gens <= args.num_reg - 1:
                    # master got results from worker. storing in reg_cdf array
                    reg_cdf[rand_gens] = data
                    rand_gens += 1

        reg_cdf.sort()

    # worker
    else:
        # ask for work
        while True:
            # worker asking for work
            comm.send(None, dest=0, tag=tags["ASK_FOR_WORK"])
            # worker sent request for work
            data = comm.recv(source=0, tag=MPI.ANY_TAG, status=status)

            if status.Get_tag() == tags["WORK"]:
                # worker got work
                # worker is computing SD
                data = rand_curve_from_background(data).std()
                # worker finished work. Sending it back
                comm.send(data, dest=0, tag=tags["WORK_DONE"])
            elif status.Get_tag() == tags["DIE"]:
                # worker die
                break

    # distribute reg_cdf and periods to workers
    reg_cdf_bc = comm.bcast(reg_cdf, root=0)
    periods_bc = comm.bcast(periods, root=0)

    # Master
    if rank == 0:
        # score each curve

        if args.verbose:
            print('computing periodicity scores')
            if args.num_per == 0:
                print('periodicity p-values not computed')

        output_data = []

        # number of generations performed
        gene_index = 0
        dead_workers = 0

        while dead_workers < num_proc - 1:
            # master looking for workers
            curve_dic = comm.recv(source=MPI.ANY_SOURCE, tag=MPI.ANY_TAG, status=status)
            dest = status.Get_source()

            if status.Get_tag() == tags["ASK_FOR_WORK"]:
                if gene_index <= len(data_df.index) - 1:

                    # master got request for work from worker
                    # get a curve
                    var = data_df.index.values[gene_index]
                    curve = data_df.loc[var]
                    # master sending work to worker
                    comm.send(curve, dest=dest, tag=tags["WORK"])

                    gene_index += 1
                    # master sent work to worker
                else:
                    # Someone did more work than they should have
                    # master telling worker to die
                    comm.send(curve, dest=dest, tag=tags["DIE"])
                    dead_workers += 1

            elif status.Get_tag() == tags["WORK_DONE"]:
                if gene_index <= len(data_df.index):
                    # master got results from worker. append to output_data
                    output_data.append(curve_dic)

        # normalize p-values and compute final score

        # join outputs
        output_df = pd.DataFrame(output_data)
        output_df.set_index("", inplace=True)
        output_df.sort_index(inplace=True)

        # compute final scores
        # replace any p-values of 0 with the minimum between either the smallest nonzero p-value or .001
        output_df[output_df == 0] = np.min([output_df[output_df != 0].min().min(), .001])
        # normalize p-values by the median value
        output_df['p_reg_norm'] = output_df['p_reg'] / output_df['p_reg'].median()
        output_df['p_per_norm'] = output_df['p_per'] / output_df['p_per'].median()
        output_df['p_tot'] = output_df['p_reg_norm'] * output_df['p_per_norm']
        output_df['dl_score'] = output_df['p_tot'] * (1 + (output_df['p_reg_norm'] / 0.001) ** 2) * (1 + ((output_df['p_per_norm'] / 0.001) ** 2))

        output_cols = ['period', 'reg_score', 'per_score', 'p_reg', 'p_reg_norm', 'p_per', 'p_per_norm', 'p_tot', 'dl_score']
        output_df = output_df[output_cols]

        # save output
        end_time = datetime.datetime.now()
        input_file_name, input_file_ext = os.path.splitext(os.path.basename(data_files))
        periods_str = ', '.join(["{:.4f}".format(per) for per in periods])

        # create output directory if necessary
        if not os.path.exists(os.path.dirname(args.output)):
            os.makedirs(os.path.dirname(args.output))

        with open(args.output, 'w') as out_file:
            out_file.write('# pyDL periodicity scores computed on %s\n' % datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
            out_file.write('# Calculation time:  %f seconds\n' % (end_time - start_time).total_seconds())
            out_file.write('# Input files:  %s\n' % os.path.abspath(data_files))
            out_file.write('# Period(s) of oscillation tested:  %s\n' % periods_str)
            out_file.write('# Number of random curves generated from background:  %i\n' % args.num_reg)
            out_file.write('# Number of curve permutations:  %i\n' % args.num_per)
            output_df.to_csv(out_file, sep='\t')

    # worker
    else:
        # ask for work
        while True:
            # worker asking for work
            comm.send(None, dest=0, tag=tags["ASK_FOR_WORK"])
            # worker sent request for work
            curve = comm.recv(source=0, tag=MPI.ANY_TAG, status=status)
            var = curve.name

            if status.Get_tag() == tags["WORK"]:
                # worker got work
                # make output array_dic
                curve_dic = {"": var}
                # compute regulator p-value
                preg = p_reg(curve, reg_cdf_bc)

                # compute periodicity p-values
                best_pper = 1
                if args.num_per > 0:
                    for per in periods_bc:
                        # generate sine and cosine vectors for use in periodicity score
                        sin_vec = gen_wave(times, per, np.sin)
                        cos_vec = gen_wave(times, per, np.cos)

                        per_cdf = gen_per_cdf(curve, args.num_per, sin_vec, cos_vec)
                        pper = p_per(curve, per_cdf, sin_vec, cos_vec)

                        if pper <= best_pper:
                            best_pper = pper
                            curve_dic["period"] = per
                            curve_dic["per_score"] = per_score(curve, sin_vec, cos_vec)
                            curve_dic["p_per"] = best_pper

                    curve_dic["reg_score"] = curve.std()
                    curve_dic["p_reg"] = preg

                    if args.verbose:
                        print('{}/{}'.format(data_df.index.get_loc(var)+1, len(data_df.index)))

                else:

                    curve_dic["period"] = np.NaN
                    curve_dic["per_score"] = np.NaN
                    curve_dic["p_per"] = np.NaN

                    # compute score
                    curve_dic["reg_score"] = curve.std()
                    curve_dic["p_reg"] = preg
                    curve_dic["p_tot"] = np.NaN
                    curve_dic["dl_score"] = np.NaN

                # worker finished work. Sending it back
                comm.send(curve_dic, dest=0, tag=tags["WORK_DONE"])
            elif status.Get_tag() == tags["DIE"]:
                # worker die
                break
