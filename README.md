# pyDL - de Lichtenberg Periodicity Test in Python

This Python module implements the periodicity detection algorithm of de Lichtenberg et. al. (https://academic.oup.com/bioinformatics/article/21/7/1164/268864) used to identify rhythmicity in gene expression profile time series data.

## Installing pyDL

```
$ git clone git@gitlab.com:biochron/pydl.git
$ cd pydl
$ ./install.sh
```

## Running pyDL

pyDL can be run in parallel using MPI and a typical run will be initiated by a terminal command:
```
$ mpiexec -n <number of processes> python pydl/pydl.py <full path to data file> -T <periods of oscillation to test> -o <full path to directory to save output>
```
* **required command-line arguments**
    * **data_files**: full path(s) to gene expression data file(s)
    * **-T, --periods**: comma-separated list specifying one or more periods i.e. 1,3,5,7
    * **-o, --output**: full path to file to save pyJTK output (should be .tsv or .txt file type)
* **optional command-line arguments**
    * **-r, --num_reg**: number of random curves for empirical regulation p-value (default: 1,000,000)
    * **-p, --num_per**: number of random curves for empirical periodicity p-value (default: 100,000)
    * **-l, --log_trans**: bool flag indicating if data requires log transform (default: True)
    * **-v, --verbose**: bool flag indicating if progress should be displayed (default: False)
* **Important note** 
    * **-p**, **--num_per** can be set to **0** to skip over computing periodicity for each curve
    
### Data Input

The pyDL module takes as input a single tab-delimited text file containing time series gene expression profiles, with rows indexed by gene names and columns indexed by time points.

|  | time 1 | time 2 | ... | time n |
| :---: | :---: | :---: | :---: | :---: |
| **gene 1** | 653.13 | 578.55 | ... | 437.58 |
| **gene 2** | 226.35 | 254.71 | ... | 345.22 |
| ... | ... | ... | ... | ... |
| **gene N** | 1274.12 | 1124.77 | ... | 929.48 |

### Output
 
The pyDL module outputs a tab delimited score file recording the estimated period (the tested period which achieved the smallest empirical p-value) intermediate scores, p-values, and the final periodicity score computed using the best period for each gene:
 
|  | period | reg_score | per_score | p_reg | p_per | p_tot | dl_score |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| **gene 1** | 24 | 19.13 | 65.45 | 0.024 | 0.001 | 0.000024 | 0.027696  |
| **gene 2** | 22 | 24.54 | 22.12 | 0.019 | 0.456 | 0.008664 | 652166.95 |
| ... | ... | ... | ... | ... | ... | ... | ... |
| **gene N** | 24 | 16.23 | 99.34 | 0.771 | 0.890 | 0.68619 | 323098121452 |

## Author

* **Francis Motta**,  **Anastasia Deckard** and **Robert Moseley**

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details


