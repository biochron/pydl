#!/usr/bin/env python

from setuptools import setup, find_packages

install_requires = []

setup(
    name='pydl',
    version='1.0',
    package_dir={'': '.'},
    packages=find_packages('.'),
    install_requires=install_requires,
    author="Francis Motta, Anastasia Deckard and Robert Moseley",
    url='https://gitlab.com/biochron/pydl'
    )